/*
#   clove
#
#   Copyright (C) 2020 Muresan Vlad
#
#   This project is free software; you can redistribute it and/or modify it
#   under the terms of the MIT license. See LICENSE.md for details.
*/
#pragma once

#include "../3rdparty/FH/src/fh.h"

#define FH_GRAPHICS_CANVAS 7

void fh_graphics_canvas_register(struct fh_program *prog);
