/*
#   clove
#
#   Copyright (C) 2021 Muresan Vlad
#
#   This project is free software; you can redistribute it and/or modify it
#   under the terms of the MIT license. See LICENSE.md for details.
*/
#ifndef CONFIG_H
#define CONFIG_H

#include "../3rdparty/FH/src/fh.h"

#include <stdbool.h>

int fh_config(struct fh_program *prog);

#endif // CONFIG_H
